﻿// test_task_1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
	setlocale(0, "");

	cout << "Исходная строка: ";

	string str;
	getline(cin, str);
	vector<char> symbols;

	transform(str.begin(), str.end(), str.begin(), tolower);

	for (auto i = str.begin(), e = str.end(); i != e; ++i)
		if (count(str.begin(), e, *i) == 1)
			symbols.push_back(*i);

	for (size_t i = 0; i < str.length(); i++)
	{
		vector<char>::iterator it = find(symbols.begin(), symbols.end(), str[i]);

		if (it == symbols.end())
			str[i] = ')';
		else
			str[i] = '(';
	}

	cout << "Результат: " << str << endl;

}